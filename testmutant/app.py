from tkinter import*
from tkinter import ttk
import random
import time
import datetime
import urllib.request  
import json 

root = Tk()
#main body
root.geometry("1350x750+0+0")
root.title('Sistema')
root.configure(background='#383838')

#JSON request
url = 'https://jsonplaceholder.typicode.com/users'  
resp = urllib.request.urlopen(url+str()).read()  
resp = json.loads(resp.decode('utf-8'))

#frames
Top = Frame(root, width=1350, height=100)
Top.pack(side=TOP)

Left = Frame(root, width=900, height=630)
Left.pack(side=LEFT)
Left.configure(background='#2E0854')

Right = Frame(root, width=400, height=630)
Right.pack(side=RIGHT)
Right.configure(background='#2E0854')

#right side frames
Sidert = Frame(Right, width=400, height=550)
Sidert.pack(side=TOP)
Sidert.configure(background='#FF1493')

Siderb = Frame(Right, width=400, height=80)
Siderb.pack(side=BOTTOM)
Siderb.configure(background='#eee')

#left side frames
Sidelt = Frame(Left, width=900, height=50)
Sidelt.pack(side=TOP)
Sidelt.configure(background='#ffffff')

Sidelb = Frame(Left, width=900, height=580)
Sidelb.pack(side=BOTTOM)
Sidelb.configure(background='#2E0854')

#labels
lblTitulo = Label(Top, font=('Calibri', 26), text="Teste Mutant", width=40)
lblTitulo.grid(row=0, column=0)
lblOpcoes = Label(Sidert, font=('Calibri', 22), fg='#FFFFFF', text="Possibilidades")
lblOpcoes.grid(row=1, column=0)
lblOpcoes.configure(background='#2E0854')

#buttons
def callback():
        
        i= 1
        for x in resp:
                if(resp.findOne({"address" : {regex : ".*suite.*"}})):
                        print("Nao Tem")        
                else:
                        treeview.bind()                       
                        treeview.insert('',i,x['name'], text = x['name'])
                        treeview.set(x['name'],'Email',x['email'])
                        treeview.set(x['name'],'Site',x['website'])
                        #treeview.set(x['name'],'Empresa',x['company.name'])
                        i= i+1

b = Button(Siderb, text="Consulta Suite", command=callback, width=40)
b.grid(row=1, column=1)
b.pack()


#treeView
treeview = ttk.Treeview(Sidelb)
treeview.pack()
treeview.config(columns = ('Email', 'Site', 'Empresa'), height= 450)
treeview.column('#0', width = 250, anchor = 'center')
treeview.heading('#0', text= 'Nome')
treeview.column('Email', width = 250, anchor = 'center')
treeview.heading('Email', text= 'Email')
treeview.column('Site', width = 250, anchor = 'center')
treeview.heading('Site', text= 'Site')
treeview.column('Empresa', width = 150, anchor = 'center')
treeview.heading('Empresa', text= 'Empresa')

i= 1
for x in resp:
        treeview.insert('',i,x['name'], text = x['name'])
        treeview.set(x['name'],'Email',x['email'])
        treeview.set(x['name'],'Site',x['website'])
        #treeview.set(x['name'],'Empresa',x['company.name'])
        i= i+1


 


#final
root.mainloop()
